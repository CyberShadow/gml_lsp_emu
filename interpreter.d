module gml_lsp_emu.interpreter;

import std.algorithm.iteration;
import std.array;
import std.conv;
import std.exception;
import std.format;
import std.stdio : stderr;
import std.typecons;

import ae.utils.array;
import ae.utils.meta;

import gml_lsp_emu.intstr;
import gml_lsp_emu.program;

interface IEnv
{
	int allocObject();
	void addGlobalObject(IntStr name, int id);
	int getObjectID(IntStr name);
	IObject getObject(int id);

	IObject getGlobal();

	Value call(IntStr name, IObject self, Value[] arguments);

	IEnv dup() const;
}

class Env : IEnv
{
	this()
	{
		instances ~= null; // Reserve object ID 0 as invalid
		global = allocObject();
	}

	IObject newObject(int id) { return new GObject(id); }

	override int allocObject()
	{
		auto id = instances.length.to!int;
		instances ~= newObject(id);
		return id;
	}

	override void addGlobalObject(IntStr name, int id)
	{
		objectSymbols[name] = id;
	}

	override int getObjectID(IntStr name)
	{
		return *(name in objectSymbols).enforce("No such object: " ~ name.toString());
	}

	override IObject getObject(int id) { return instances[id]; }
	override IObject getGlobal() { return getObject(global); }

	final override Env dup() const { auto copy = cast(Env) create(); dupTo(copy); return copy; }

protected:
	abstract Env create() const;

	void dupTo(Env copy) const
	{
		copy.self = self;
		copy.local = local;
		copy.global = global;
		copy.objectSymbols = objectSymbols
			.byKeyValue
			.map!(kv => tuple(kv.key, int(kv.value)))
			.assocArray;
		copy.instances = instances.amap!(o => o.dup);
	}

	int self, local, global;

	int[IntStr] objectSymbols;
	IObject[] instances;
}

struct Stack(T, size_t maxSize)
{
	T[maxSize] store;
	size_t length;

	void push(T value)
	{
		if (length == store.length)
			assert(false, "Stack overflow");
		store[length++] = value;
	}

	ref T pop()
	{
		return store[--length];
	}
}

struct State
{
	size_t pc;
	IEnv env;
	Stack!(Value, 4) stack;
	IObject self, local;

	void assign(ref Program.Expression expr, Value value)
	{
		final switch (expr.kind)
		{
			case Program.Expression.Kind.none:
				assert(false);
			case Program.Expression.Kind.value:
				throw new Exception("Trying to assign to immediate");
			case Program.Expression.Kind.symbol:
				throw new Exception("Trying to assign to symbol");
			case Program.Expression.Kind.object:
				throw new Exception("Trying to assign to object");
			case Program.Expression.Kind.dot:
			{
				auto object = evalExpr(*expr.dot.object);
				enforce(object.type == Value.Type.object, "Object expected");
				object.get!IObject.put(expr.dot.symbol, value);
				break;
			}
			case Program.Expression.Kind.function_:
				throw new Exception("Unexpected function");
			case Program.Expression.Kind.index1:
			{
				auto origIndexee = evalExpr(*expr.index.indexee);
				auto indexee = origIndexee;
				if (indexee.type == Value.Type.undefined)
					indexee = Value(Value.Type.array1);
				enforce(indexee.type == Value.Type.array1, "Indexing non-array");
				auto index1 = evalExpr(*expr.index.index1).to!int.int32;
				if (indexee.array1.length < index1 + 1)
					indexee.array1.length = index1 + 1;
				indexee.array1[index1] = value;
				if (indexee !is origIndexee)
					assign(*expr.index.indexee, indexee);
				break;
			}
			case Program.Expression.Kind.index2:
			{
				auto origIndexee = evalExpr(*expr.index.indexee);
				auto indexee = origIndexee;
				if (indexee.type == Value.Type.undefined)
					indexee = Value(Value.Type.array2);
				enforce(indexee.type == Value.Type.array2, "Indexing non-array");
				auto index1 = evalExpr(*expr.index.index1).to!int.int32;
				auto index2 = evalExpr(*expr.index.index2).to!int.int32;
				if (indexee.array2.length < index1 + 1)
					indexee.array2.length = index1 + 1;
				if (indexee.array2[index1].length < index2 + 1)
					indexee.array2[index1].length = index2 + 1;
				indexee.array2[index1][index2] = value;
				if (indexee !is origIndexee)
					assign(*expr.index.indexee, indexee);
				break;
			}
			case Program.Expression.Kind.not:
				throw new Exception("Trying to assign to not");
			case Program.Expression.Kind.list:
				throw new Exception("Trying to assign to function call");
			case Program.Expression.Kind.pop:
				throw new Exception("Trying to assign to pop");
		}
	}

	private Value evalBinary(string op)(ref Program.Expression expr)
	{
		enforce(expr.list.length == 3);
		auto a = evalExpr(expr.list[1]);
		auto b = evalExpr(expr.list[2]);
		return a.opBinary!op(b);
	}

	private Value evalCast(T)(ref Program.Expression expr)
	{
		enforce(expr.list.length == 2);
		return evalExpr(expr.list[1]).to!T();
	}

	private Value evalAssertNeq(T)(ref Program.Expression expr)
	{
		enforce(expr.list.length == 3);
		auto a = evalExpr(expr.list[1]).to!T;
		auto b = evalExpr(expr.list[2]).to!T;
		enforce(a.get!T != b.get!T, "assert_neq failed");
		return b;
	}

	Value evalExpr(ref Program.Expression expr)
	{
		scope(failure) stderr.writefln("Error evaluating expression %s:", expr);
		final switch (expr.kind)
		{
			case Program.Expression.Kind.none:
				assert(false);
			case Program.Expression.Kind.value:
				return expr.value;
			case Program.Expression.Kind.symbol:
				switch (expr.symbol.index)
				{
					case literal!"self"  .index: return Value(self.enforce("No self"));
					case literal!"local" .index: return Value(local);
					case literal!"global".index: return Value(env.getGlobal());
					default:
						return Value(env.getObjectID(expr.symbol));
				}
			case Program.Expression.Kind.object:
				return Value(env.getObject(evalExpr(*expr.object).to!int.get!int));
			case Program.Expression.Kind.dot:
			{
				auto object = evalExpr(*expr.dot.object);
				enforce(object.type == Value.Type.object, "Object expected");
				auto value = object.object.get(expr.dot.symbol);
				return value;
			}
			case Program.Expression.Kind.function_:
				throw new Exception("Unexpected function");
			case Program.Expression.Kind.index1:
			{
				auto indexee = evalExpr(*expr.index.indexee);
				enforce(indexee.type == Value.Type.array1, "Indexing non-array");
				auto index1 = evalExpr(*expr.index.index1).to!int.int32;
				enforce(indexee.array1.length > index1);
				return indexee.array1[index1];
			}
			case Program.Expression.Kind.index2:
			{
				auto indexee = evalExpr(*expr.index.indexee);
				enforce(indexee.type == Value.Type.array2, "Indexing non-array");
				auto index1 = evalExpr(*expr.index.index1).to!int.int32;
				auto index2 = evalExpr(*expr.index.index2).to!int.int32;
				enforce(indexee.array2.length > index1);
				enforce(indexee.array2[index1].length > index2);
				return indexee.array2[index1][index2];
			}
			case Program.Expression.Kind.not:
				return Value(!evalExpr(*expr.not).to!bool.boolean);
			case Program.Expression.Kind.list:
			{
				enforce(expr.list.length);
				switch (expr.list[0].kind)
				{
					case Program.Expression.Kind.symbol:
						switch (expr.list[0].symbol.index)
						{
							case literal!"==".index: return evalBinary!"=="(expr);
							case literal!"!=".index: return evalBinary!"!="(expr);
							case literal!"<" .index: return evalBinary!"<" (expr);
							case literal!">" .index: return evalBinary!">" (expr);
							case literal!"<=".index: return evalBinary!"<="(expr);
							case literal!">=".index: return evalBinary!">="(expr);

							case literal!"+".index: return evalBinary!"+"(expr);
							case literal!"-".index: return evalBinary!"-"(expr);
							case literal!"*".index: return evalBinary!"*"(expr);
							case literal!"/".index: return evalBinary!"/"(expr);

							case literal!"~".index:
							{
								enforce(expr.list.length == 2);
								auto value = evalExpr(expr.list[1]);
								switch (value.type)
								{
									case Value.Type.boolean: return Value(!value.boolean);
									case Value.Type.int16  : return Value(cast(int16)~int32(value.int16)  );
									case Value.Type.int32  : return Value(           ~      value.int32  );
									default: throw new Exception("Unknown type for ~");
								}
							}

							case literal!"var".index:
								enforce(expr.list.length == 2);
								enforce(expr.list[1].kind == Program.Expression.Kind.value);
								return expr.list[1].value;

							case literal!"bool"  .index: return evalCast!bool  (expr);
							case literal!"int16" .index: return evalCast!int16 (expr);
							case literal!"int32" .index: return evalCast!int32 (expr);
							case literal!"double".index: return evalCast!double(expr);

							case literal!"assert_neq:int16".index: return evalAssertNeq!int16(expr);

							default:
								throw new Exception("Unknown operation: " ~ expr.list[0].symbol.toString());
						}
					case Program.Expression.Kind.function_:
					{
						enforce(expr.list[0].function_.type == literal!"int32", "Type is not int32?");
						Value[] args;
						foreach (arg; expr.list[1 .. $])
							args ~= evalExpr(arg);
						return env.call(expr.list[0].function_.name, self, args);
					}
					default:
						throw new Exception("Function call on %s".format(expr.list[0].kind));
				}
			}
			case Program.Expression.Kind.pop:
				return stack.pop;
		}
	}
}

Value run(Program program, Env env, IObject self, Value[] arguments)
{
	Value[] oldArguments = new Value[arguments.length];
	foreach (i, argument; arguments)
	{
		auto argumentName = IntStr("argument" ~ text(i));
		oldArguments[i] = self.enforce("No self to store arguments").get(argumentName);
		self.put(argumentName, argument);
	}
	scope (success)
		foreach (i, argument; oldArguments)
			self.put(IntStr("argument" ~ text(i)), argument);

	State state;
	state.env = env;
	state.pc = 0;
	state.self = self;
	state.local = new GObject;

	while (true)
	{
		auto instr = &program.instructions[state.pc];
		scope(failure) stderr.writefln("Error at %s:%d:", instr.loc.file, instr.loc.line);
		final switch (instr.op)
		{
			case Program.Instruction.Op.none:
				assert(false);
			case Program.Instruction.Op.if_:
			{
				auto result = state.evalExpr(instr.if_.expr);
				if (result.to!bool.boolean)
					state.pc = instr.if_.target;
				else
					state.pc++;
				break;
			}
			case Program.Instruction.Op.goto_:
				state.pc = instr.goto_.target;
				break;
			case Program.Instruction.Op.push:
				state.stack.push(state.evalExpr(instr.push.expr));
				state.pc++;
				break;
			case Program.Instruction.Op.pop:
				//enforce(state.stack.length, "Stack underflow"); // TODO?
				if (state.stack.length)
					state.stack.pop();
				state.pc++;
				break;
			case Program.Instruction.Op.assign:
				state.assign(*instr.assign.target, state.evalExpr(*instr.assign.source));
				state.pc++;
				break;
			case Program.Instruction.Op.call:
				enforce(instr.call.kind == Program.Expression.Kind.list);
				state.evalExpr(instr.call);
				state.pc++;
				break;
			case Program.Instruction.Op.retVar:
				return state.evalExpr(instr.ret.expr);
			case Program.Instruction.Op.exit:
				return Value(Value.Type.void_);
		}
	}
}
