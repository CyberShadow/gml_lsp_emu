module gml_lsp_emu.parser;

import std.algorithm.comparison;
import std.algorithm.searching;
import std.ascii;
import std.conv;
import std.exception;
import std.stdio : stderr;
import std.string;

import ae.utils.array;
import ae.utils.graphics.draw;
import ae.utils.text;

import gml_lsp_emu.program;
import gml_lsp_emu.intstr;

Program parse(string s, string fileName = null)
{
	Program program;

	size_t[string] labels;

	struct Target
	{
		size_t source;
		string name;
		void function(ref Program.Instruction i, size_t target) setter;
	}
	Target[] targets;

	foreach (lineNr, line; s.splitWithSuffix("\n"))
	{
		scope(failure) stderr.writefln("Error on line %d (%(%s%)):", 1 + lineNr, [line]);
		line = line[0 .. $-1];
		if (line.startsWith("0x") && line.endsWith(":"))
			labels[line[0 .. $-1]] = program.instructions.length;
		else
		if (line.skipOver("    "))
		{
			Program.Instruction i;
			i.loc = Program.Loc(fileName, 1 + cast(int)lineNr);
			if (line.skipOver("if "))
			{
				i.op = Program.Instruction.Op.if_;
				i.if_.expr = readExpression(line);
				line.skipOver(" goto ").enforce();
				targets ~= Target(program.instructions.length, line, (ref Program.Instruction i, size_t target) { i.if_.target = target; });
			}
			else
			if (line.skipOver("goto "))
			{
				i.op = Program.Instruction.Op.goto_;
				targets ~= Target(program.instructions.length, line, (ref Program.Instruction i, size_t target) { i.goto_.target = target; });
			}
			else
			if (line.skipOver("push "))
			{
				i.op = Program.Instruction.Op.push;
				i.push.expr = parseExpression(line);
			}
			else
			if (line == "pop")
				i.op = Program.Instruction.Op.pop;
			else
			if (line.skipOver("call "))
			{
				i.op = Program.Instruction.Op.call;
				i.call = parseExpression(line);
			}
			else
			if (line.skipOver("ret var "))
			{
				i.op = Program.Instruction.Op.retVar;
				i.push.expr = parseExpression(line);
			}
			else
			if (line == "exit")
				i.op = Program.Instruction.Op.exit;
			else
			if (line.indexOf(" = ") >= 0)
			{
				auto p = line.findSplit(" = ");
				i.op = Program.Instruction.Op.assign;
				i.assign.target = [parseExpression(p[0])].ptr;
				i.assign.source = [parseExpression(p[2])].ptr;
			}
			else
				throw new Exception("Unknown instruction: " ~ line);
			program.instructions ~= i;
		}
		else
			throw new Exception("Unknown line: " ~ line);
	}

	foreach (target; targets)
	{
		auto offset = *(target.name in labels).enforce("No such label: " ~ target.name);
		target.setter(program.instructions[target.source], offset);
	}

	return program;
}

string readWord(ref string s)
{
	auto p = s.representation.countUntil!(c => !!c.among(' ', '(', ')', '[', ']', ',', '.'));
	string result;
	if (p < 0)
		p = s.length;
	enforce(p > 0, "Word expected");
	result = s[0 .. p];
	s = s[p .. $];
	return result;
}

Program.Expression readExpression(ref string s)
{
	// auto os = s;
	enforce(s.length);
	Program.Expression expr;
	switch (s[0])
	{
		case '!':
			if (s.startsWith("!="))
				goto default;
			s = s[1 .. $];
			expr.kind = Program.Expression.Kind.not;
			expr.not = [readExpression(s)].ptr;
			break;
		case '(':
			s = s[1 .. $];
			expr.kind = Program.Expression.Kind.list;
			while (true)
			{
				if (s.skipOver(')')) // decompiler glitch for call instructions
					goto end;
				expr.list ~= readExpression(s);
				enforce(s.length);
				auto c = s.shift;
				switch (c)
				{
					case ' ':
						continue;
					case ')':
						goto end;
					default:
						throw new Exception("Unknown character in list after reading sub-expression: " ~ c ~ s);
				}
			}
			assert(false); // DMD bug
		case '"':
		{
			expr.kind = Program.Expression.Kind.value;
			expr.value.type = Value.Type.string;
			s = s[1 .. $];
			string str;
		strLoop:
			while (true)
			{
				char c = s.shift;
				switch (c)
				{
					case '"':
						break strLoop;
					case '\\':
						c = s.shift;
						switch (c)
						{
							case '"':
							case '\\':
								str ~= c;
								continue;
							case 'r': str ~= '\r'; continue;
							case 'n': str ~= '\n'; continue;
							default:
								throw new Exception("Unknown escape");
						}
					default:
						str ~= c;
				}
			}
			expr.value = Value(str);
			break;
		}
		case '0':
			..
		case '9':
		{
			expr.kind = Program.Expression.Kind.value;
			auto w = readWord(s);
			while (s.skipOver("."))
				w ~= "." ~ readWord(s);
			enforce(w.length);
			switch (w[$ - 1])
			{
				case '0':
					..
				case '9':
					expr.value = Value(w.to!int32);
					break;
				case 's':
					expr.value = Value(w[0 .. $-1].to!int16);
					break;
				case 'd':
					expr.value = Value(w[0 .. $-1].to!double);
					break;
				default:
					throw new Exception("Unknown numeric type: " ~ w);
			}
			break;
		}
		case '-':
			if (s.length > 1 && s[1].isDigit)
				goto case '0';
			goto default;
		case '[':
			s = s[1 .. $];
			expr.kind = Program.Expression.Kind.object;
			expr.object = [readExpression(s)].ptr; // should be an int32
			enforce(s.skipOver("]"));
			break;
		case 'p':
			if (s.skipOver("pop"))
			{
				expr.kind = Program.Expression.Kind.pop;
				break;
			}
			goto default;
		default:
		{
			string w;
			if (s.startsWith("self:"))
			{
				w = "self";
				s = s[4 .. $];
			}
			else
			if (s.startsWith("local:"))
			{
				w = "local";
				s = s[5 .. $];
			}
			else
				w = readWord(s);
			if (s.skipOver("[]:"))
			{
				expr.kind = Program.Expression.Kind.function_;
				expr.function_.name = IntStr(w);
				expr.function_.type = IntStr(readWord(s));
			}
			else
			{
				expr.kind = Program.Expression.Kind.symbol;
				expr.symbol = IntStr(w);
			}
		}
	}

end:
	while (true)
	{
		if (s.skipOver("["))
		{
			Program.Expression next;
			next.index.indexee = [expr].ptr;
			next.index.index1 = [readExpression(s)].ptr;
			if (s.skipOver(", "))
			{
				next.kind = Program.Expression.Kind.index2;
				next.index.index2 = [readExpression(s)].ptr;
			}
			else
				next.kind = Program.Expression.Kind.index1;
			enforce(s.skipOver("]"));
			expr = next;
		}
		else
		if (s.skipOver(".") || s.skipOver(":"))
		{
			Program.Expression next;
			next.kind = Program.Expression.Kind.dot;
			next.dot.object = [expr].ptr;
			next.dot.symbol = IntStr(readWord(s));
			expr = next;
		}
		else
			break;
	}

	return expr;
}

Program.Expression parseExpression(string s)
{
	auto expr = readExpression(s);
	enforce(!s.length, "Trailing text in expression: " ~ s);
	return expr;
}

Value parseValue(string s)
{
	auto expr = parseExpression(s);
	enforce(expr.kind == Program.Expression.Kind.value, "Not a value");
	return expr.value;
}
