module gml_lsp_emu.game;

import std.algorithm.comparison;
import std.algorithm.searching;
import std.conv;
import std.exception;
import std.file;
import std.string;

import ae.utils.json;
import ae.utils.meta;

import gml_lsp_emu.interpreter;
import gml_lsp_emu.intstr;
import gml_lsp_emu.parser;
import gml_lsp_emu.program;

class Game
{
	string dir;
	GameEnv env;

	this(string dir)
	{
		this.dir = dir;
		if (!env)
			env = new GameEnv;
	}

	class GameEnv : Env
	{
		override GameEnv create() const { return (cast()this)._create(); }
		private final GameEnv _create() { return new GameEnv; }

		Value call(IntStr name, IObject self, Value[] arguments)
		{
			// Conversion
			if (name == intstr!"real")
			{
				enforce(arguments.length == 1);
				if (arguments[0].type == Value.Type.string)
					return Value(arguments[0].string.to!double);
				else
					return arguments[0].to!double;
			}
			else
			if (name == intstr!"string")
			{
				enforce(arguments.length == 1);
				auto v = arguments[0];
				switch (v.type)
				{
					case Value.Type.string:  return v;
					case Value.Type.int16:   return Value(v.int16.to!string);
					case Value.Type.int32:   return Value(v.int32.to!string);
					case Value.Type.double_: return Value(v.double_.to!string);
					default: throw new Exception("Don't know how to convert %s to string".format(v.type));
				}
			}
			else
			// String
			if (name == intstr!"string_length")
			{
				enforce(arguments.length == 1);
				return Value(cast(int32)arguments[0].string.length);
			}
			else
			if (name == intstr!"string_count")
			{
				enforce(arguments.length == 2);
				enforce(arguments[0].string.length == 1, "Not implemented");
				return Value(arguments[1].string.representation.count!(c => c == arguments[0].string[0]));
			}
			else
			if (name == intstr!"string_char_at")
			{
				enforce(arguments.length == 2);
				auto s = arguments[0].string;
				auto p = arguments[1].to!int32.int32;
				p--;
				if (p < 0 || p >= s.length)
					return Value("");
				return Value(s[p .. p + 1]);
			}
			else
			if (name == intstr!"string_copy")
			{
				enforce(arguments.length == 3);
				auto s = arguments[0].string;
				auto p = arguments[1].to!int32.int32;
				auto c = arguments[2].to!int32.int32;
				p--;
				return Value(s[max(0, p) .. min($, p + c)]);
			}
			else
			if (name == intstr!"string_delete")
			{
				enforce(arguments.length == 3);
				auto s = arguments[0].string;
				auto p = arguments[1].to!int32.int32;
				auto c = arguments[2].to!int32.int32;
				p--;
				return Value(s[0 .. max(0, p)] ~ s[min($, p + c) .. $]);
			}
			else
			// Custom
			{
				auto script = getScript(name);
				auto program = &getProgram(script.code);

				GObject tempObj;
				if (!self && arguments)
					self = tempObj = new GObject;
				scope (success)
					if (tempObj)
						assert(tempObj.properties.byValue.all!(v => v.type == Value.Type.undefined),
							"Lost writes to temporary self");
				return run(*program, this, self, arguments);
			}
		}
	}

	struct JsonScript
	{
		string code;
	}
	private JsonScript[string] jsonScripts;
	ref JsonScript getScript(IntStr name)
	{
		return jsonScripts.require(name.toString(),
			(dir ~ "/script/" ~ name.toString ~ ".json")
			.readText
			.jsonParse!JsonScript
		);
	}

	private Program[string] programs;
	ref Program getProgram(string name)
	{
		return programs.require(name,
			(dir ~ "/code/" ~ name ~ ".gml.lsp")
			.readText
			.I!(s => filterProgram(name, s))
			.parse(name ~ ".gml.lsp")
		);
	}

	protected string filterProgram(string name, string s)
	{
		return s;
	}

	final Value runFunction(string name, IObject self, Value[] arguments)
	{
		return run(getProgram(name), env, self, arguments);
	}
}
