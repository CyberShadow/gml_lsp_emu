module gml_lsp_emu.mkdot;

import std.algorithm.searching;
import std.exception;
import std.file;
import std.process;
import std.stdio;
import std.string;

void main(string[] args)
{
	foreach (fn; args[1..$])
	{
		auto dot = fn ~ ".dot";
		auto f = File(dot, "wb");
		f.writeln(`digraph {`);
		f.writeln(`	graph [bgcolor=black] ratio="compress";`);
		f.writeln(`	node [shape=box style=filled fillcolor="#222222" fontcolor="#FFFFFF" penwidth=2];`);
		f.writeln(`	edge [color=white fontcolor=white fontname="helvetica"];`);
		f.writeln(`	node [fontname="monospace"];`);
		f.writeln(``);

		string current;
		string[] lines;

		enum Last
		{
			none,
			if_,
			ifNot,
		}
		Last last;

		string nodeName(string label) { return label[1 .. $]; }

		void flush()
		{
			if (!current)
				return;
			f.writefln(`	%s [label=%s]`, nodeName(current), format("%(%s%)", [(lines ~ "").join("\n")]).replace(`\n`, `\l`));
			current = null;
			lines = null;
			last = Last.none;
		}

		foreach (line; fn.readText.splitLines)
		{
			if (!line.length)
				continue;
			scope(failure) stderr.writeln("Error parsing line: " ~ line);
			if (line.startsWith("0x"))
			{
				enforce(line.endsWith(":"));
				auto label = line[0 .. $-1];
				if (current)
				{
					f.writefln(`	%s -> %s [color=%s]`,
						nodeName(current),
						nodeName(label),
						["white", "red", "green"][last],
					);
					flush();
				}
				current = label;
			}
			else
			if (line.startsWith("    "))
			{
				if (!current)
				{
					if (line.startsWith("    goto "))
						continue; // unreachable garbage gotos
					static int counter;
					current = format("unreachable_%d", counter++);
				}
				enforce(last == Last.none, "Instruction after branch");
				if (line.startsWith("    if "))
				{
					if (lines.length)
						lines ~= "";
					if (line.skipOver("    if !"))
					{
						line = "    if " ~ line;
						last = Last.ifNot;
					}
					else
						last = Last.if_;
					auto parts = line.findSplit(" goto ");
					line = parts[0];
					auto label = parts[2];
					f.writefln(`	%s -> %s [color=%s]`,
						nodeName(current),
						nodeName(label),
						[null, "green", "red"][last],
					);
				}
				else
				if (line.startsWith("    goto "))
				{
					auto label = line.findSplit(" goto ")[2];
					f.writefln(`	%s -> %s [color=cyan]`,
						nodeName(current),
						nodeName(label),
					);
					flush();
					continue;
				}
				else
				if (line.startsWith("    exit"))
				{
					lines ~= line[4 .. $];
					flush();
					continue;
				}
				lines ~= line[4 .. $];
			}
			else
				throw new Exception("Unknown line");
		}
		flush();
		f.writeln("}");
		f.close();

		enforce(spawnProcess(["dot", "-Tsvg", "-O", dot]).wait() == 0);
	}
}
