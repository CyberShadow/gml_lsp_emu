module gml_lsp_emu.intstr;

struct IntStr
{
@safe nothrow:
	this(string s)
	{
		this.index = put(s);
	}

	string toString() const
	{
		return get(index);
	}

	bool opCast(T)() const
	if (is(T == bool))
	{
		return index != 0;
	}

	alias Index = ushort;
	Index index;

	this(Index index) { this.index = index; }

private:
static:
	shared string[] strings = [null];
	shared Index[string] lookup;

	string get(Index index)
	{
		return strings[index];
	}

	Index put(string s) @trusted
	{
		if (s is null)
			return 0; // Distinguish null from ""
		Index ret = void;

		(*cast(Index[string]*)&lookup).update(s,
			{
				ret = cast(Index)strings.length;
				*(cast()&strings) ~= s;
				return ret;
			},
			(ref Index old)
			{
				return ret = old;
			},
		);
		return ret;
	}
}

enum inull = IntStr.init;

immutable string[] builtinStrings = [
	null,

	"==",
	"!=",
	"<",
	">",
	"<=",
	">=",

	"+",
	"-",
	"*",
	"/",
	"~",

	"var",

	"bool",
	"int16",
	"int32",
	"double",

	"self",
	"local",
	"global",

	"assert_neq:int16",
];

static this()
{
	foreach (i, s; builtinStrings)
	{
		auto interned = IntStr(s);
		assert(interned.index == i);
	}
}

template literal(string str)
{
	enum literal = (){
		foreach (i, s; builtinStrings)
			if (s == str)
			{
				IntStr interned;
				interned.index = cast(IntStr.Index)i;
				return interned;
			}
		assert(false, "Not a built-in string: " ~ str);
	}();
}

template intstr(string str)
{
	IntStr intstr;
	static this()
	{
		intstr = IntStr(str);
	}
}
