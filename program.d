module gml_lsp_emu.program;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.array;
import std.conv;
import std.format;
import std.meta : AliasSeq;
import std.typecons;

import gml_lsp_emu.intstr;

alias int16 = short;
alias int32 = int;

// TODO: Get rid of this and replace it with an IEnv method to
// read/write object properties by object ID and property name, then
// extend that to arrays
interface IObject
{
	int getID();
	Value get(IntStr name); // Returns undefined if not present
	void put(IntStr name, Value value);
	IObject dup() const pure;
}

class GObject : IObject
{
	int id;
	Value[IntStr] properties;

	this(int id = 0) pure { this.id = id; }
	int getID() { return id; }
	Value get(IntStr name) { return properties.get(name, Value.init); }
	void put(IntStr name, Value value) { properties[name] = value; }

	GObject dup() const pure
	{
		GObject result = new GObject(id);
		result.properties = properties
			.byKeyValue
			.map!(kv => tuple(kv.key, kv.value.dup))
			.assocArray;
		return result;
	}
}

struct Value
{
	enum Type
	{
		undefined, // indicates absence of value
		void_,
		boolean,
		int16,
		int32,
		double_,
		string,
		array1,
		array2,
		object,
	}
	Type type;

	alias types = AliasSeq!(void, void, bool, .int16, .int32, double, .string, Value[], Value[][], IObject);

	enum undefined = Value(Type.undefined);
	enum void_ = Value(Type.void_);

	// @property ref inout(Type) type() return inout { return type_; }

	template typeOf(T)
	{
		static if (is(T == void))
			enum typeOf = Type.void_;
		else
		static if (is(T == bool))
			enum typeOf = Type.boolean;
		else
		static if (is(T == .int16))
			enum typeOf = Type.int16;
		else
		static if (is(T == .int32))
			enum typeOf = Type.int32;
		else
		static if (is(T == double))
			enum typeOf = Type.double_;
		else
		static if (is(T == .string))
			enum typeOf = Type.string;
		else
		static if (is(T == Value[]))
			enum typeOf = Type.array1;
		else
		static if (is(T == Value[][]))
			enum typeOf = Type.array2;
		else
		static if (is(T : IObject))
			enum typeOf = Type.object;
		else
			static assert(false, "No type for D type " ~ T.stringof);
	}

	private union Store
	{
		bool boolean;
		.int16 int16;
		.int32 int32;
		double double_;
		.string string;
		// TODO: this is wrong, arrays are reference types - resizing
		// one reference should resize all
		Value[] array1;
		Value[][] array2;
		IObject object;
	}
	private Store store;

	this(Type type) pure { this.type = type; }
	this(bool value) pure { this.type = Type.boolean; this.store.boolean = value; }
	this(short value) pure { this.type = Type.int16; this.store.int16 = value; }
	this(int value) pure { this.type = Type.int32; this.store.int32 = value; }
	this(double value) pure { this.type = Type.double_; this.store.double_ = value; }
	this(.string value) pure { this.type = Type.string; this.store.string = value; }
	this(Value[] value) pure { this.type = Type.array1; this.store.array1 = value; }
	this(Value[][] value) pure { this.type = Type.array2; this.store.array2 = value; }
	this(IObject value) pure { this.type = Type.object; this.store.object = value; }

	invariant() { assert(type <= Type.max); }

	ref inout(T) get(T)() inout
	{
		assert(type == typeOf!T, "Type mismatch for get");
		static if (is(T == void))
			return;
		else
		static if (is(T == bool))
			return store.boolean;
		else
		static if (is(T == .int16))
			return store.int16;
		else
		static if (is(T == .int32))
			return store.int32;
		else
		static if (is(T == double))
			return store.double_;
		else
		static if (is(T == .string))
			return store.string;
		else
		static if (is(T == Value[]))
			return store.array1;
		else
		static if (is(T == Value[][]))
			return store.array2;
		else
		static if (is(T : IObject))
			return store.object;
		else
			static assert(false);
	}

	alias boolean = get!bool;
	alias int16 = get!(.int16);
	alias int32 = get!(.int32);
	alias double_ = get!double;
	alias string = get!(.string);
	alias array1 = get!(Value[]);
	alias array2 = get!(Value[][]);
	alias object = get!IObject;

	.string toString() const
	{
		final switch (type)
		{
			case Type.undefined: return "undefined";
			case Type.void_    : return "void";
			case Type.boolean  : return boolean.text;
			case Type.int16    : return int16  .text ~ "s";
			case Type.int32    : return int32  .text;
			case Type.double_  : return double_.text ~ "d";
			case Type.string   : return format("%(%s%)", [string]);
			case Type.array1   : return array1 .text;
			case Type.array2   : return array2 .text;
			case Type.object   : return object .text;
		}
	}

	Value dup() const pure
	{
		final switch (type)
		{
			case Type.undefined: return this;
			case Type.void_    : return this;
			case Type.boolean  : return this;
			case Type.int16    : return this;
			case Type.int32    : return this;
			case Type.double_  : return this;
			case Type.string   : return this; // string are immutable
			case Type.array1   : return Value(array1.map!(v => v.dup).array);
			case Type.array2   : return Value(array2.map!(a => a.map!(v => v.dup).array).array);
			case Type.object   : return Value(object.dup);
		}
	}


	Value to(T)()
	{
		if (this.type == typeOf!T)
			return this;
		static if (is(T == bool))
			if (this.type == Value.Type.void_)
				return Value(false);
		switch (this.type)
		{
			case Value.Type.int16   : return Value(this.int16   .to!T);
			case Value.Type.int32   : return Value(this.int32   .to!T);
			case Value.Type.double_ : return Value(this.double_ .to!T);
			default:
				throw new Exception("Don't know how to cast %s to %s".format(this.type, T.stringof));
		}
	}

	Value to(Value.Type type)
	{
		if (this.type == type)
			return this;
		switch (type)
		{
			case Value.Type.int16   : return to!(.int16);
			case Value.Type.int32   : return to!(.int32);
			case Value.Type.double_ : return to!double;
			default:
				throw new Exception("Don't know how to cast %s to %s".format(this.type, type));
		}
	}

	Value opBinary(.string op)(Value b) const
	{
		alias a = this;

		if (a.type == Type.undefined || b.type == Type.undefined)
			throw new Exception("Operation on undefined");
		if (a.type == Type.void_ || b.type == Type.void_)
			throw new Exception("Operation on void");

		switch (a.type)
		{
			case Value.Type.boolean :
				switch (b.type)
				{
					case Value.Type.boolean : static if (is(typeof(mixin(`a.boolean ` ~ op ~ `b.boolean `)))) return Value(mixin(`a.boolean ` ~ op ~ `b.boolean `)); else goto default;
					default: throw new Exception("Unknown type for operation: %s %s %s".format(a.type, op, b.type));
				}
			case Value.Type.int16   :
				switch (b.type)
				{
					case Value.Type.int16   :
					{
						// Special case here to avoid C/D integer promotion
						auto result = mixin(`a.int16   ` ~ op ~ `b.int16   `);
						static if (is(typeof(result) == int))
							if (result >= short.min && result <= short.max)
								return Value(cast(short)result);
						return Value(result);
					}
					case Value.Type.int32   : return Value(mixin(`a.int16   ` ~ op ~ `b.int32   `));
					case Value.Type.double_ : return Value(mixin(`a.int16   ` ~ op ~ `b.double_ `));
					default: throw new Exception("Unknown type for operation: %s %s %s".format(a.type, op, b.type));
				}
			case Value.Type.int32   :
				switch (b.type)
				{
					case Value.Type.int16   : return Value(mixin(`a.int32   ` ~ op ~ `b.int16   `));
					case Value.Type.int32   : return Value(mixin(`a.int32   ` ~ op ~ `b.int32   `));
					case Value.Type.double_ : return Value(mixin(`a.int32   ` ~ op ~ `b.double_ `));
					default: throw new Exception("Unknown type for operation: %s %s %s".format(a.type, op, b.type));
				}
			case Value.Type.double_ :
				switch (b.type)
				{
					case Value.Type.int16   : return Value(mixin(`a.double_ ` ~ op ~ `b.int16   `));
					case Value.Type.int32   : return Value(mixin(`a.double_ ` ~ op ~ `b.int32   `));
					case Value.Type.double_ : return Value(mixin(`a.double_ ` ~ op ~ `b.double_ `));
					default: throw new Exception("Unknown type for operation: %s %s %s".format(a.type, op, b.type));
				}
			case Value.Type.string  :
				switch (b.type)
				{
					case Value.Type.string  : static if (is(typeof(mixin(`a.string  ` ~ op ~ `b.string  `)))) return Value(mixin(`a.string  ` ~ op ~ `b.string  `)); else goto default;
					default: throw new Exception("Unknown type for operation: %s %s %s".format(a.type, op, b.type));
				}
			case Value.Type.array1  :
				switch (b.type)
				{
					case Value.Type.array1  : static if (is(typeof(mixin(`a.array1  ` ~ op ~ `b.array1  `)))) return Value(mixin(`a.array1  ` ~ op ~ `b.array1  `)); else goto default;
					default: throw new Exception("Unknown type for operation: %s %s %s".format(a.type, op, b.type));
				}
			case Value.Type.array2  :
				switch (b.type)
				{
					case Value.Type.array2  : static if (is(typeof(mixin(`a.array2  ` ~ op ~ `b.array2  `)))) return Value(mixin(`a.array2  ` ~ op ~ `b.array2  `)); else goto default;
					default: throw new Exception("Unknown type for operation: %s %s %s".format(a.type, op, b.type));
				}
			default: throw new Exception("Unknown type for operation: %s %s %s".format(a.type, op, b.type));
		}
	}

	hash_t toHash() const /*@safe pure*/ nothrow
	{
		return cast(hash_t)type * cast(hash_t)0xD6D76CCF3DC8FD13 + valueHash();
	}

	private hash_t valueHash() const /*@safe pure*/ nothrow
	{
		final switch (type)
		{
			case Type.undefined: return 0;
			case Type.void_    : return 0;
			case Type.boolean  : return boolean.hashOf;
			case Type.int16    : return int16  .hashOf;
			case Type.int32    : return int32  .hashOf;
			case Type.double_  : return double_.hashOf;
			case Type.string   : return string .hashOf;
			case Type.array1   : return array1 .hashOf;
			case Type.array2   : return array2 .hashOf;
			case Type.object   : return object .hashOf;
		}
	}

	bool opEquals(Value b) const
	{
		if (type != b.type)
			return false;
		if (type.among(Type.undefined, Type.void_))
			return true;
		return opBinary!"=="(b).boolean;
	}

	int opCmp(Value b) const
	{
		if (type != b.type)
			return type < b.type ? -1 : 1;
		return this == b ? 0 : opBinary!"<"(b).boolean ? -1 : 1;
	}
}

struct Program
{
	struct Loc
	{
		string file;
		int line;
	}

	struct Expression
	{
		enum Kind
		{
			none,
			value,
			symbol, // object symbol (name to object/instance number)
			object, // get object or instance by number
			dot,
			function_,
			index1,
			index2,
			not,
			list,
			pop,
		}
		Kind kind;

		union
		{
			Value value;
			IntStr symbol;
			Expression* object;

			struct Dot
			{
				Expression* object;
				IntStr symbol;
			}
			Dot dot;

			struct Function
			{
				IntStr name;
				IntStr type;
			}
			Function function_;

			struct Index
			{
				Expression* indexee, index1, index2;
			}
			Index index;

			Expression* not;
			Expression[] list;
		}

		string toString() const
		{
			final switch (kind)
			{
				case Kind.none     : assert(false);
				case Kind.value    : return value .toString();
				case Kind.symbol   : return symbol.toString();
				case Kind.object   : return "[" ~ object.toString() ~ "]";
				case Kind.dot      : return dot.object.toString() ~ "." ~ dot.symbol.toString();
				case Kind.function_: return function_.name.toString() ~ "[]:" ~ function_.type.toString();
				case Kind.index1   : return index.indexee.toString() ~ "[" ~ index.index1.toString() ~ "]";
				case Kind.index2   : return index.indexee.toString() ~ "[" ~ index.index1.toString() ~ ", " ~ index.index2.toString() ~ "]";
				case Kind.not      : return "!" ~ not.toString();
				case Kind.list     : return "(%-(%s %))".format(list);
				case Kind.pop      : return "pop";
			}	
		}
	}

	struct Instruction
	{
		Loc loc;

		enum Op
		{
			none,
			if_,
			goto_,
			push,
			pop,
			assign,
			call,
			retVar,
			exit,
		}
		Op op;

		union
		{
			struct If
			{
				Expression expr;
				size_t target;
			}
			If if_;

			struct Goto
			{
				size_t target;
			}
			Goto goto_;

			struct Push
			{
				Expression expr;
			}
			Push push;

			struct Assign
			{
				Expression* target, source;
			}
			Assign assign;

			Expression call;

			struct Ret
			{
				Expression expr;
			}
			Ret ret;
		}
	}
	Instruction[] instructions;
}
